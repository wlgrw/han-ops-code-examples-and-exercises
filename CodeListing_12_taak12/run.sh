#!/bin/bash
#

## \file run.sh
## \brief Sample code to demonstrate the correct use of semaphores
## \author Remko Welling (WLGRW) 
## \date 24-3-2019
## \version 1.0
##
## Program description
## ===================
## This program shows that two processes that are not related can share a single semaphore. 
## One task is creating a semaphore and delivers the inital value while the other task waits 
## until the semaphore is created.
## 
## Question: 
## ---------
## Why is the consumer waiting to print the buffer?
## 
## Assignment:
## -----------
## 1 - Study the code of task1.c, task2.c and the bash script.
## 2 - Comment the code describing the operation
## 3 - Explain for the class how the program works and why the semaphores do work.


# 1. Starting two indepedent tasks that use the same mutex
echo " "
echo " 1: two indepedent tasks that use the same mutex." 
echo "------------------------------------------------------"
read -p "Press [Enter] key to execute..."
./taak1 & ./taak2

# 2. Starting two indepedent tasks that use the same mutex while starting order is reversed
echo " "
echo " 2: two indepedent tasks that use the same mutex in reversed order of starting." 
echo "------------------------------------------------------"
read -p "Press [Enter] key to execute..."
./taak2 & sleep 2 && ./taak1

# 3. Starting two indepedent tasks that use the same mutex while starting order is reversed
echo " "
echo " 3: Start task2 without starting task1. To stop taak2 press [ctl-C]." 
echo "------------------------------------------------------"
read -p "Press [Enter] key to execute..."
./taak2

