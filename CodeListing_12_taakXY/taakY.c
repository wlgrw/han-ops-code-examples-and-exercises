/*!

 \file taakY.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will wait for "taakX" to be started and print "Y moet na X"
 
 Operation
 =========
 Compile the code using "make" and run it using bash script "run.sh".
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Answer the questions given in bash script "run.sh".


 */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

int main()
{
    int i = 0;
    sem_t *psync;
    while ((psync = sem_open("sem_sync", 0)) == SEM_FAILED)
    {
        printf("Geen sync bij TaakY\n");
        usleep(50000);
    }
    sem_wait(psync);
    printf("Y moet na X\n");
    sem_close(psync);
    sem_unlink("sem_sync");
    return 0;
}