/*!

 \file forkedSemaphore.c
 \brief Sample code to demonstrate the faulty use of semaphores
 \author Remko Welling (WLGRW) remko.welling@han.nl
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will have a child and parent printing when they start and when they stop to the screen.
 
 The objective is that parent and child will wait for each other and that parent and child will not
 interrupt each other. However due to wrong coding this will not be the case.
 
 +------------------+
 |KA is interrupted:|
 +------------------+
 |Child start KA    |
 |Parent start KA   |
 |Child stopt KA    |
 |Parent stopt KA   |
 |Child start KA    |
 |Parent start KA   |
 |Child stopt KA    |
 |Child start KA    |
 |Parent stopt KA   |
 |Child stopt KA    |
 |Child start KA    |
 |Child stopt KA    |
 |Parent start KA   |
 |Child start KA    |
 |Parent stopt KA   |
 |Child stopt KA    |
 |Child start KA    |
 |Parent start KA   |
 |Child stopt KA    |
 |Parent stopt KA   |
 |Child start KA    |
 |Child stopt KA    |
 |Parent start KA   |
 |etc.              |
 +------------------+
 
 Operation
 =========
 Compile the code using "make" and run it.
 
 Question: 
 ---------
 Why are parent and child printing "randomly" not waiting for each other.
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Identify the cause why the semaphores are not working.
 4 - Explain for the class how the program works and why the semaphores do not work.


 */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <wait.h>
#include <time.h>

#define MAXLOOP 10

int main(void)
{
    sem_t mutex;
    int i = 0;
    
    sem_init(&mutex, 1, 1);

    switch (fork())
    {
    case -1: /*error*/
        printf("Geen child.\n");
        break;

    case 0: /*Child process*/
        for (i = 0; i < MAXLOOP; i++)
        {
            sem_wait(&mutex);
            printf("Child start KA\n");
            usleep(10000);
            printf("Child stopt KA\n");
            sem_post(&mutex);
            usleep(13000);
        }
        break;

    default : /*Parent process*/
        for (i = 0; i < MAXLOOP; i++)
        {
            sem_wait(&mutex);
            printf("Parent start KA\n");
            usleep(10000);
            printf("Parent stopt KA\n");
            sem_post(&mutex);
            usleep(30000);
        }
        wait(NULL);
        sem_destroy(&mutex);
        break;
    }
    return 0;
}