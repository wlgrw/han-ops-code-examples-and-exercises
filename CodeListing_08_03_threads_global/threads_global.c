// Multithreading example using a global variable for the return information
#include <pthread.h>
#include <stdio.h>

void *Tenfold(void *arg);  // Thread-function prototype
double result;             // Note: using a global variable

int main(void) {
  pthread_t thread_id;
  double number = 123.456;
  double *myResult;
  
  pthread_create(&thread_id, NULL, Tenfold, (void*) &number);
  
  // We expect a pointer value and need a double pointer to receive it:
  pthread_join(thread_id, (void**) &myResult);
  printf("Result: %lf\n", *myResult);
  
  // Discouraged alternative:
  //pthread_join(thread_id, NULL);
  //printf("Result: %lf\n", result);
  
  return 0;
}

void *Tenfold(void *arg) {
  result = *(double*) arg;  // Cast argument (back) to double* and dereference
  result *= 10;
  pthread_exit((void*) &result);  // Return the address of the global variable
}
