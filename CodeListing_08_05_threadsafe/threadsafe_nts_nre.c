int t;
void swap(int *x, int *y) {
  t = *x;
  *x = *y;
  *y = t;
}
