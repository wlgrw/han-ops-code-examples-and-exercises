/*!

 \file semaforeEnThreading.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This example is using a producer and a consumer of data. 
 Using a delay we make the producer generate data that is stored in to a buffer. 
 Because of the use of the semaphore "synchr" we delay the printing of the buffer by the consumer
 until the buffer is filled with data by the producer.
 
 Inital buffer content: 
 +------------------------------+
 |123456789                     |
 +------------------------------+

 Printed output by consumer:
 +------------------------------+
 |abcdefghijklmnopqrstuvwxyz    |
 +------------------------------+
 
 Operation
 =========
 Compile the code using "make" and run it.
 
 Question: 
 ---------
 Why is the consumer waiting to print the buffer?
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Identify a possible reason that will cause the consumer not to wait for thebuffer to be filled?
 4 - Explain for the class how the program works and why the semaphores do work.


 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <time.h>

void *Prod(void *pArgP);
void *Cons(void *pArgC);

char acBuffer[30] = "0123456789";
sem_t synchr;

int main(void)
{
    pthread_t threadProd, threadCons;
    sem_init(&synchr, 0, 0);         
    (void)pthread_create(&threadProd, NULL, Prod, NULL);
    (void)pthread_create(&threadCons, NULL, Cons, NULL);
    (void)pthread_join(threadProd, NULL);
    (void)pthread_join(threadCons, NULL);
    sem_destroy(&synchr);
    return 0;
}

void *Prod(void *pArgP)
{
    char cL;
    int i = 0;
    usleep(30000);
    for (cL = 'a'; cL <= 'z'; cL++, i++)
    {
        acBuffer[i] = cL;
    }
    acBuffer[i] = '\0';
    sem_post(&synchr);
    return NULL;
}

void *Cons(void *pArgC)
{
    sem_wait(&synchr);
    printf("Uitvoer van consument: %s\n", acBuffer);
    return NULL;
}