#!/bin/bash
# 
# \brief Running shared memory example
# \author WLGRW

echo "BASH: Starting shm_write: ./shm_write \"Hello There\""
./shm_write "Hello There"
echo ""

echo "BASH: lookup SHM: ls -l /dev/shm/mySHM"
ls -l /dev/shm/mySHM
echo ""

echo "BASH: running first time shm_read: ./shm_read"
./shm_read
echo 

echo "BASH: running second time shm_read: ./shm_read"
./shm_read
echo 
