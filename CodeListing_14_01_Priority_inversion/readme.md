# Priority inversion in FreeRTOS

This file is presented as part of H-ESE-OPS class. 

## Prerequisite

For this activity you may use your HAN-IOT-Node.

## Activities

   1. Go To Arduino: https://www.arduino.cc/en/software 
   2. Download Arduino: https://downloads.arduino.cc/arduino-1.8.13-windows.zip 
   3. Unzip file in D:\temp or C:\temp depending on your preference.
   4. Make the Arduino installation protable: 
      - In directory “D:\temp\arduino-1.8.13-windows\arduino-1.8.13” create directory “portable”
   4. Start Arduino.exe in D:\apps\arduino-1.8.13-windows\arduino-1.8.13.
   5. Configure Arduino IDE: In “File > Preferences”:
      - Enable all compiler messages and warnings.
   6. In “sketch > Use library > Add library ” add library “FreeRTOS”.
   7. Open "FreeRTOS_PriorityInversion.ino" in the Arduino IDE just installed.

