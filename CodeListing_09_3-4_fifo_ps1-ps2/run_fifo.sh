#!/bin/bash
# 
# \brief starting two consecutively programs
# \author WLGRW

echo "Starting fifo_ps1 in the background"
./fifo_ps1 &
echo "Starting fifo_ps2"
./fifo_ps2