#define  _XOPEN_SOURCE 500  // For S_IFIFO in C99+
#include <stdio.h>          // perror(), printf()
#include <stdlib.h>         // exit()
#include <unistd.h>         // write(), close(), unlink()
#include <sys/stat.h>       // mkfifo()
#include <fcntl.h>          // open(), O_* constants

int main(void) {
  int fd;
  char message[20] = "This is my message";
  
  //mkfifo("FIFO", S_IFIFO|0666);          // Create FIFO; permissions: u/g/o r/w
  fd = open("FIFO", O_WRONLY);           // Open FIFO write-only
  if (fd < 0)
  {
    printf("FIFO Open failed.\n");
    return -1;
  }
  write(fd, &message, sizeof(message));  // Write message to FIFO
  close(fd);                             // Close FIFO
  //unlink("FIFO");                        // Mark FIFO for removal
  
  return 0;
}
