/*!

 \file semaforeEnThreading_2.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 With this program we want the producer to generate data multiple times that is being re-read by the consumer
 while witing for the data to be generated. Therefore we need 2 semaphores. 
 Semaphore "Prod-Mag" (start value 1) allows the producer to start producing.
 After that the producer has produced, it has to wait until the consumer consumed the data and has signed to be ready.
 Consumer makes semaphore "ProdMag" 1 after consumption.
 Semaphore "ConsMag" (starting value 0) makes the consumer to wait until the producer is ready producing.
 De producer maakt hiertoe ConsMag telkens 1 na het produceren.

 Program execution will deliver following output:

 Output of consumer: 123456789
 Output of consumer: ABCDEFGH
 Output of consumer: QRSTUVW
 Output of consumer: abcdef
 Output of consumer: qrstu

 Without the use of semaphores the program output will be:

 Output of consumer: xxx
 Output of consumer: xxx
 Output of consumer: xxx
 Output of consumer: xxx
 Output of consumer: xxx
 
 Operation
 =========
 Compile the code using "make" and run it.
 
 Question: 
 ---------
 Why is the consumer waiting to print the buffer?
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Identify a possible reason that will cause the consumer not to wait for producer?
 4 - Explain for the class how the program works and why the semaphores do work.


 */
 

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <time.h>

void *Prod(void *pArgP);
void *Cons(void *pArgC);

char acBuffer[30] = "xxx";
sem_t ProdMag, ConsMag; // 

int main(void)
{
    pthread_t threadProd, threadCons;
    sem_init(&ProdMag, 0, 1);
    sem_init(&ConsMag, 0, 0);
    (void)pthread_create(&threadProd, NULL, Prod, NULL);
    (void)pthread_create(&threadCons, NULL, Cons, NULL);
    (void)pthread_join(threadProd, NULL);
    (void)pthread_join(threadCons, NULL);
    sem_destroy(&ProdMag);
    sem_destroy(&ConsMag);
    return 0;
}

void *Prod(void *pArgP)
{
    char cL = '1';
    int i, j, iMax = 9;
    for (i = 0; i < 5; i++, iMax--)
    {
        usleep(12000);
        sem_wait(&ProdMag);
        for (j = 0; j < iMax; j++)
        {
            acBuffer[j] = cL + j;
        }
        acBuffer[j] = '\0';
        sem_post(&ConsMag);
        cL = cL + 16;
    }
    return NULL;
}
void *Cons(void *pArgC)
{
    int i;
    for (i = 0; i < 5; i++)
    {
        sem_wait(&ConsMag);
        printf("Output of consumer: %s\n", acBuffer);
        sem_post(&ProdMag);
    }
    return NULL;
}
